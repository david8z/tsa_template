from django.db import models
from categorias.models import Categoria
from nichos.models import Nicho

from django.db.models.signals import pre_save

from django.utils.text import slugify

class TextoSeo(models.Model):
    title = models.CharField(max_length=75)
    text = models.TextField(max_length=5000)

    slug = models.SlugField(unique=True, max_length=75, blank=True)

    volumen = models.PositiveIntegerField(db_index=True)

    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, related_name='textos_seo', null=True, blank=True)
    nicho = models.ForeignKey(Nicho, on_delete=models.CASCADE, related_name='textos_seo', null=True, blank=True)


    class Meta:
        verbose_name = 'Texto Seo'
        verbose_name_plural = 'Textos Seo'

    def __str__(self):
        return "{}".format(self.title)
    
    def save(self, *args, **kwargs):
        """
        Comprobación de que tiene nicho o categoría asignada.
        """
        if (not self.categoria) == (not self.nicho):
            raise Exception("El texto seo ha de tener asignada o un nicho o una categoria.")

        super(TextoSeo, self).save(*args, **kwargs)

def texto_seo_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de generar el slug field al crear el texto seo.
    """
    if not instance.slug:
        instance.slug = slugify(instance.title)

pre_save.connect(texto_seo_pre_save_receiver, sender=TextoSeo)
