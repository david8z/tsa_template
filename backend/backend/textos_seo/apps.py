from django.apps import AppConfig


class TextosSeoConfig(AppConfig):
    name = 'textos_seo'
