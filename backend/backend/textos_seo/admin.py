from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export import resources

from textos_seo.models import TextoSeo

class TextoSeoResource(resources.ModelResource):
    class Meta:
        model = TextoSeo

@admin.register(TextoSeo)
class TextoSeoAdmin(ImportExportModelAdmin):
    list_display = ('slug', 'volumen', 'categoria', 'nicho')
    list_filter = ('categoria', 'nicho')
    ordering = ('-volumen',)


