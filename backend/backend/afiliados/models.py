from django.db import models


from django.db.models.signals import pre_save, pre_delete

from django.utils.text import slugify


class Afiliado(models.Model):
    imagen = models.FileField(upload_to="afiliados")
    alt_imagen = models.TextField(max_length=500)

    slug = models.SlugField(unique=True, max_length=50, blank=True)

    width = models.PositiveIntegerField()

    nombre = models.CharField(max_length=30)

    imagen_boton = models.ImageField(upload_to="afiliados_boton", null=True, blank=True)
    alt_imagen_boton = models.TextField(max_length=500, null=True, blank=True)

    class Meta:
        verbose_name = 'Afiliado'
        verbose_name_plural = 'Afiliados'

    def __str__(self):
        return "{}".format(self.nombre)

def afiliado_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de generar el slug field al crear la categoria.
    """
    if not instance.slug:
        instance.slug = slugify(instance.nombre)


def afiliado_pre_delete_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de eliminar la imagen de media una vez eliminado el objeto.
    """
    if instance.imagen:
        instance.imagen.delete()
 
    if instance.imagen_boton:
        instance.imagen_boton.delete()

pre_delete.connect(afiliado_pre_delete_receiver, sender=Afiliado)
pre_save.connect(afiliado_pre_save_receiver, sender=Afiliado)
