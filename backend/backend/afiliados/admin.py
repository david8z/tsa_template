from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export import resources

from afiliados.models import Afiliado

class AfiliadoResource(resources.ModelResource):
    class Meta:
        model = Afiliado


@admin.register(Afiliado)
class AfiliadoAdmin(ImportExportModelAdmin):
    list_display = ('nombre', 'slug')


    


# admin.site.register(Afiliado, AfiliadoAdmin)
