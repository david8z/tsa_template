"""
backend URL Configuration
"""
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path, include

from rest_framework import routers
from nichos.views import NichoViewSet
from categorias.views import CategoriaViewSet
from productos.views import ProductoViewSet

ROUTER = routers.DefaultRouter()
ROUTER.register(r'nichos', NichoViewSet, basename="Nichos")
ROUTER.register(r'categorias', CategoriaViewSet, basename="Categorias")
ROUTER.register(r'productos', ProductoViewSet, basename="Productos")


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(ROUTER.urls))
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
