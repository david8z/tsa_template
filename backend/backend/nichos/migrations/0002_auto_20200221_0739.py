# Generated by Django 2.2.7 on 2020-02-21 06:39

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nichos', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='nicho',
            name='estrellas',
            field=models.FloatField(default=0, validators=[django.core.validators.MaxValueValidator(5.0), django.core.validators.MinValueValidator(0.0)]),
        ),
        migrations.AddField(
            model_name='nicho',
            name='valoraciones',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
