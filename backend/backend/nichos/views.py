from django.shortcuts import render
from django.db.models import Prefetch

from nichos.models import Nicho
from categorias.models import Categoria
from textos_seo.models import TextoSeo
from nichos.serializers import NichoSerializer, NichoListSerializer
from rest_framework import viewsets

from rest_framework.response import Response

class NichoViewSet(viewsets.ModelViewSet):
    """
    API endpoint que permite Buscar todas la información del nicho.
    """
    serializer_class = NichoSerializer
    http_method_names = ['get', 'put']
    lookup_field = 'slug'

    def get_queryset(self):
        return Nicho.objects.all()
    
    def list(self, request):
        queryset = self.get_queryset()
        serializer = NichoListSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, slug):
        queryset = Nicho.objects.filter(slug=slug).prefetch_related(
            Prefetch(
                "categorias",
                queryset=Categoria.objects.filter(nicho__slug=slug),
            ),
            Prefetch(
                "textos_seo",
                queryset=TextoSeo.objects.filter(nicho__slug=slug).order_by('-volumen'),

            )
        )
        serializer = NichoSerializer(queryset, many=True)
        return Response(serializer.data)
    
    def put(self, request, *args, **kwargs):
        nicho = Nicho.objects.filter(slug=request.data.get("nicho"))[0]
        if nicho.valoraciones == 0:
            nicho.estrellas = 1
        else:
            nicho.estrellas = (nicho.estrellas * nicho.valoraciones / (nicho.valoraciones +1)) + (request.data.get("estrellas")/ (nicho.valoraciones +1))
        nicho.valoraciones = nicho.valoraciones + 1
        nicho.save()
        return Response({"estrellas":nicho.estrellas, "valoraciones": nicho.valoraciones})

