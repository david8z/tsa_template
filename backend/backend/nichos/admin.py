from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export import resources

from nichos.models import Nicho

class NichoResource(resources.ModelResource):
    class Meta:
        model = Nicho

@admin.register(Nicho)
class NichoAdmin(ImportExportModelAdmin):
    list_display = ('nombre', 'volumen', 'slug')
    ordering = ('-volumen',)

    fieldsets = (
        ('Main Fields', {'fields': ('nombre', 'main_title', 'main_text')}),
        ('Meta Tags', {'fields': ('meta_title', 'meta_description')}),
        ('General', {'fields': ('afiliados', 'volumen', 'slug')}),
        ('Extras', {'fields': ('estrellas', 'valoraciones')})
    )
