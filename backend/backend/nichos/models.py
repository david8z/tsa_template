from django.db import models

from afiliados.models import Afiliado

from django.db.models.signals import pre_save

from django.core.validators import MinValueValidator, MaxValueValidator

from django.utils.text import slugify


class Nicho(models.Model):
    meta_title = models.CharField(max_length=60)
    meta_description = models.TextField(max_length=158)

    main_title = models.CharField(max_length=75)
    main_text = models.TextField(max_length=1000)
  
    afiliados = models.ManyToManyField(Afiliado)

    nombre = models.CharField(max_length=50, null=True)
    volumen = models.PositiveIntegerField(null=True)

    estrellas = models.FloatField(validators=[MaxValueValidator(5.0), MinValueValidator(0.0)], default=0, blank=True)
    valoraciones = models.PositiveIntegerField(default=0, blank=True)
  
    slug = models.SlugField(unique=True, max_length=50, blank=True)

    class Meta:
        verbose_name = 'Nicho'
        verbose_name_plural = 'Nichos'

    def __str__(self):
        return "{}".format(self.nombre)

def nicho_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de generar el slug field al crear el nicho.
    """
    if not instance.slug:
        instance.slug = slugify(instance.nombre)

pre_save.connect(nicho_pre_save_receiver, sender=Nicho)
