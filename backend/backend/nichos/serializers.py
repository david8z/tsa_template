"""DOC STRING"""
import django.core.checks

from rest_framework import serializers

from afiliados.models import Afiliado
from nichos.models import Nicho
from categorias.models import Categoria
from textos_seo.models import TextoSeo

####################
##NICHO SERIALIZER##
####################
@django.core.checks.register('rest_framework.serializers')
class AfiliadoSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    class Meta:
        model = Afiliado
        fields = ['alt_imagen', 'imagen', 'width']

@django.core.checks.register('rest_framework.serializers')
class CategoriaSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    class Meta:
        model = Categoria
        fields = ['top', 'slug', 'volumen', 'color', 'imagen', 'alt_imagen', 'home_title', 'main_text']

@django.core.checks.register('rest_framework.serializers')
class TextoSeoSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    class Meta:
        model = TextoSeo
        fields = ['title', 'text', 'volumen']

@django.core.checks.register('rest_framework.serializers')
class NichoSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    afiliados = AfiliadoSerializer(many=True)
    categorias = CategoriaSerializer(many=True)
    textos_seo = TextoSeoSerializer(many=True)
    class Meta:
        model = Nicho
        fields = ['meta_title', 'meta_description', 'main_title', 'main_text', 'afiliados', 'nombre', 'categorias', 'textos_seo', 'estrellas', 'valoraciones']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'},
        }

#########################
##NICHO LIST SERIALIZER##
#########################
@django.core.checks.register('rest_framework.serializers')
class NichoListSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    class Meta:
        model = Nicho
        fields = ['nombre', 'volumen', 'slug']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'},
        }