"""DOC STRING"""
import django.core.checks

from rest_framework import serializers
from afiliados.models import Afiliado
from productos.models import Producto

#######################
##PRODUCTO SERIALIZER##
#######################
@django.core.checks.register('rest_framework.serializers')
class AfiliadoSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    class Meta:
        model = Afiliado
        fields = ['imagen_boton', 'alt_imagen_boton', 'nombre']

@django.core.checks.register('rest_framework.serializers')
class ProductoSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    afiliado = AfiliadoSerializer(many=False)
    class Meta:
        model = Producto
        fields = ['enlace', 'slug', 'estrellas', 'valoraciones', 'imagen', 'alt_imagen', 'nombre', 
        'description', 'precio', 'precio_anterior', 'afiliado', 'caracteristica']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'},
        }

############################
##PRODUCTO LIST SERIALIZER##
############################
@django.core.checks.register('rest_framework.serializers')
class ProductoListSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    class Meta:
        model = Producto
        fields = ['slug']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'},
        }