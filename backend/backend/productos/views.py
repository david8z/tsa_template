from rest_framework import viewsets
from rest_framework.response import Response

from categorias.models import Categoria
from productos.models import Producto
from productos.serializers import ProductoListSerializer, ProductoSerializer


class ProductoViewSet(viewsets.ModelViewSet):
    """
    API endpoint que permite Buscar todas la información del nicho.
    """
    serializer_class = ProductoSerializer
    http_method_names = ['get']
    lookup_field = 'slug'

    def get_queryset(self):
        return Producto.objects.all()
    
    def list(self, request):
        queryset = self.get_queryset()
        serializer = ProductoListSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, slug):
        queryset = Producto.objects.filter(categorias__slug=slug)
        serializer = ProductoSerializer(queryset, many=True)
        return Response(serializer.data)
