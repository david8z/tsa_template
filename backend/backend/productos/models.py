from django.db import models

from django.db.models.signals import pre_save, pre_delete

from django.core.validators import MinValueValidator, MaxValueValidator


from django.utils.text import slugify

from afiliados.models import Afiliado
from categorias.models import Categoria

class Producto(models.Model):
    enlace = models.TextField(max_length=500)
    slug = models.SlugField(unique=True, max_length=100, blank=True)

    estrellas = models.FloatField(validators=[MaxValueValidator(5.0), MinValueValidator(0.0)], default=0)
    valoraciones = models.PositiveIntegerField(default=0)

    imagen = models.ImageField(upload_to="productos")
    alt_imagen = models.TextField(max_length=500)

    nombre = models.TextField(max_length=250)
    description = models.TextField(max_length=300)

    precio = models.FloatField(validators=[MinValueValidator(0.0)], null=True, blank=True)
    precio_anterior = models.FloatField(validators=[MinValueValidator(0.0)], null=True, blank=True)

    caracteristica = models.CharField(max_length=50, null=True, blank=True)

    afiliado = models.ForeignKey(Afiliado, on_delete=models.CASCADE, related_name='afiliado')

    categorias = models.ManyToManyField(Categoria)

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    def __str__(self):
        return "{}".format(self.slug)

def producto_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de generar el slug field al crear el objeto..
    """
    if not instance.slug:
        instance.slug = slugify(instance.nombre)

def producto_pre_delete_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de eliminar la imagen de media una vez eliminado el objeto.
    """
    if instance.imagen:
        instance.imagen.delete()

pre_save.connect(producto_pre_save_receiver, sender=Producto)
pre_delete.connect(producto_pre_delete_receiver, sender=Producto)
