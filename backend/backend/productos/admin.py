from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export import resources

from productos.models import Producto

class ProductoResource(resources.ModelResource):
    class Meta:
        model = Producto

@admin.register(Producto)
class ProductoAdmin(ImportExportModelAdmin):
    list_display = ('slug', 'precio', 'afiliado', 'categorias_list')
    list_filter = ('afiliado', 'categorias')
    ordering = ('-precio',)
    
    def categorias_list(self, obj):
        return "[ " + ",\n".join([str(p) for p in obj.categorias.all()]) +" ]"
    
    fieldsets = (
        ('Main Fields', {'fields': ('nombre', 'description', 'enlace', 'caracteristica')}),
        ('Precio', {'fields': ('precio', 'precio_anterior')}),
        ('Valoración', {'fields': ('estrellas', 'valoraciones')}),
        ('Imagen', {'fields': ('imagen', 'alt_imagen')}),
        ('General', {'fields': ('afiliado', 'categorias', 'slug')}),
    )



