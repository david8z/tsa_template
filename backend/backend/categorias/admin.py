from django.contrib import admin

from import_export.admin import ImportExportModelAdmin
from import_export import resources

from categorias.models import Categoria

class CategoriaResource(resources.ModelResource):
    class Meta:
        model = Categoria

@admin.register(Categoria)
class CategoriaAdmin(ImportExportModelAdmin):
    list_display = ('slug', 'volumen', 'nicho', 'top')
    list_filter = ('top', 'nicho')
    ordering = ('-volumen',)

    fieldsets = (
        ('Textos', {'fields': ('main_title', 'home_title', 'main_text')}),
        ('Meta Tags', {'fields': ('meta_title', 'meta_description')}),
        ('Card', {'fields': ('imagen', 'alt_imagen', 'color')}),
        ('General', {'fields': ('nicho', 'volumen', 'top', 'slug')}),
        ('Extras', {'fields': ('estrellas', 'valoraciones')})
    )
