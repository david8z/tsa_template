import binascii
from PIL import Image
import numpy as np
import scipy
import scipy.misc
import scipy.cluster
from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000

from django.db import models

from django.db.models.signals import pre_save, pre_delete

from django.core.validators import MinValueValidator, MaxValueValidator

from django.utils.text import slugify

from nichos.models import Nicho


COLORES = [(173, 217, 219), (255, 243, 232), (255, 222, 116), (178, 201, 171)]

class Categoria(models.Model):
    top = models.BooleanField(default=False, db_index=True)
    slug = models.SlugField(unique=True, max_length=50, blank=True)

    volumen = models.PositiveIntegerField(db_index=True)

    color = models.CharField(max_length=6, blank=True)

    imagen = models.ImageField(upload_to="categorias")
    alt_imagen = models.TextField(max_length=500)

    meta_title = models.CharField(max_length=60)
    meta_description = models.TextField(max_length=158)

    estrellas = models.FloatField(validators=[MaxValueValidator(5.0), MinValueValidator(0.0)], default=0, blank=True)
    valoraciones = models.PositiveIntegerField(default=0, blank=True)

    main_title = models.CharField(max_length=75)
    main_text = models.TextField(max_length=1000)

    home_title = models.CharField(max_length=50)

    nicho = models.ForeignKey(Nicho, on_delete=models.CASCADE, related_name='categorias')


    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'

    def __str__(self):
        return "{}".format(self.slug)




def color_diference(color):
    color1_rgb = sRGBColor(color[0], color[1], color[2])

    res = [0, 0, 0, 0]
    for i, co in enumerate(COLORES):
        color2_rgb = sRGBColor(co[0], co[1], co[2])

        # Convert from RGB to Lab Color Space
        color1_lab = convert_color(color1_rgb, LabColor)

        # Convert from RGB to Lab Color Space
        color2_lab = convert_color(color2_rgb, LabColor)

        # Find the color difference
        delta_e = delta_e_cie2000(color1_lab, color2_lab)
        res[i] = delta_e
    return res


def closest_node(nodes):
    nodes = np.asarray(nodes)
    dist_2 = np.apply_along_axis(color_diference, 1, nodes)
    aux = dist_2/dist_2.sum(axis=0)

    return np.argmin(np.min(aux,axis=0))



def categoria_pre_save_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de generar el slug field al crear la categoria.
    """
    if not instance.color:
        print(type(instance.imagen))    
        NUM_CLUSTERS = 10

        im = Image.open('../media/categorias/'+str(instance.imagen))
        im = im.resize((150, 150))
        ar = np.asarray(im)
        shape = ar.shape
        ar = ar.reshape(scipy.product(shape[:2]), shape[2]).astype(float)

        codes, _ = scipy.cluster.vq.kmeans(ar, NUM_CLUSTERS)

        np_colour = np.array(codes)

        selection = closest_node(np_colour)

        instance.color = binascii.hexlify(bytearray(int(c) for c in COLORES[selection])).decode('ascii').upper()

    if not instance.slug:
        instance.slug = slugify(instance.home_title)


def categoria_pre_delete_receiver(sender, instance, *args, **kwargs):
    """
    Metodo encargado de eliminar la imagen de media una vez eliminado el objeto.
    """
    if instance.imagen:
        instance.imagen.delete()

pre_save.connect(categoria_pre_save_receiver, sender=Categoria)
pre_delete.connect(categoria_pre_delete_receiver, sender=Categoria)
