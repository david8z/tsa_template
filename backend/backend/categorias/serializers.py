"""DOC STRING"""
import django.core.checks

from rest_framework import serializers
from textos_seo.models import TextoSeo
from categorias.models import Categoria

####################
##NICHO SERIALIZER##
####################
@django.core.checks.register('rest_framework.serializers')
class TextoSeoSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    class Meta:
        model = TextoSeo
        fields = ['title', 'text', 'volumen', 'slug']

@django.core.checks.register('rest_framework.serializers')
class CategoriaSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    textos_seo = TextoSeoSerializer(many=True)
    class Meta:
        model = Categoria
        fields = ['meta_title', 'meta_description', 'main_title', 'textos_seo', 'volumen', 'estrellas', 'valoraciones', 'slug']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'},
        }

#########################
##NICHO LIST SERIALIZER##
#########################
@django.core.checks.register('rest_framework.serializers')
class CategoriaListSerializer(serializers.HyperlinkedModelSerializer):
    """DOCSTRING"""
    class Meta:
        model = Categoria
        fields = ['main_title', 'volumen', 'slug']
        extra_kwargs = {
            'url': {'lookup_field': 'slug'},
        }