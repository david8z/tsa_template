from django.shortcuts import render
from django.db.models import Prefetch

from categorias.models import Categoria
from textos_seo.models import TextoSeo

from categorias.serializers import CategoriaSerializer, CategoriaListSerializer
from rest_framework import viewsets

from rest_framework.response import Response

class CategoriaViewSet(viewsets.ModelViewSet):
    """
    API endpoint que permite Buscar todas la información del nicho.
    """
    serializer_class = CategoriaSerializer
    http_method_names = ['get', 'put']
    lookup_field = 'slug'

    def get_queryset(self):
        return Categoria.objects.all()
    
    def list(self, request):
        queryset = self.get_queryset()
        serializer = CategoriaListSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, slug):
        queryset = Categoria.objects.filter(slug=slug).prefetch_related(
            Prefetch(
                "textos_seo",
                queryset=TextoSeo.objects.filter(categoria__slug=slug).order_by('-volumen'),
            )
        )
        serializer = CategoriaSerializer(queryset, many=True)
        return Response(serializer.data)
    
    def put(self, request, *args, **kwargs):
        print(request)
        categoria = Categoria.objects.filter(slug=request.data.get("slug"))[0]
        if categoria.valoraciones == 0:
            categoria.estrellas = 1
        else:
            categoria.estrellas = (categoria.estrellas * categoria.valoraciones / (categoria.valoraciones +1)) + (request.data.get("estrellas")/ (categoria.valoraciones +1))
        categoria.valoraciones = categoria.valoraciones + 1
        categoria.save()
        return Response({"estrellas":categoria.estrellas, "valoraciones": categoria.valoraciones})
