import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _309816bf = () => interopDefault(import('../pages/aviso-legal.vue' /* webpackChunkName: "pages/aviso-legal" */))
const _1d7293fc = () => interopDefault(import('../pages/contacto.vue' /* webpackChunkName: "pages/contacto" */))
const _add4d638 = () => interopDefault(import('../pages/politica-de-cookies.vue' /* webpackChunkName: "pages/politica-de-cookies" */))
const _0ea8b1f5 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _d06f0f2e = () => interopDefault(import('../pages/_categoria_pilar/index.vue' /* webpackChunkName: "pages/_categoria_pilar/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/aviso-legal",
    component: _309816bf,
    name: "aviso-legal"
  }, {
    path: "/contacto",
    component: _1d7293fc,
    name: "contacto"
  }, {
    path: "/politica-de-cookies",
    component: _add4d638,
    name: "politica-de-cookies"
  }, {
    path: "/",
    component: _0ea8b1f5,
    name: "index"
  }, {
    path: "/:categoria_pilar",
    component: _d06f0f2e,
    name: "categoria_pilar"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
