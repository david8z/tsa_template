export const state = () => ({
  home: {}
})

export const mutations = {
  home (state, newValue) {
    state.home = {
      'meta_title': newValue.meta_title,
      'meta_description': newValue.meta_description,
      'main_title': newValue.main_title,
      'main_text': newValue.main_text,
      'afiliados': newValue.afiliados,
      'nombre': newValue.nombre,
      'categorias': newValue.categorias,
      'textos_seo': newValue.textos_seo,
      'estrellas': newValue.estrellas,
      'valoraciones': newValue.valoraciones
    }
  },
  home_valoraciones (state, newValue) {
    state.home.estrellas = parseFloat((Math.round(newValue.estrellas * 2) / 2).toFixed(1))
    state.home.valoraciones = newValue.valoraciones
  }
}

export const actions = {
  async nuxtServerInit (vuexContext, context) {
    // WE SET USER OR ANONYMOUS USER AND CART AND WISHLIST
    vuexContext.commit('home', (await this.$axios.get('/api/nichos/' + process.env.nicho_json + '/')).data[0])
  }
}

export const getters = {
  home: state => state.home
}
