import colors from 'vuetify/es5/util/colors'

require('dotenv').config()

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      lang: 'es'
    },
    title: process.env.DEFAULT_TITLE,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.DEFAULT_DESCRIPTION }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'preconnect', href: 'https://cdn.jsdelivr.net' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/jsonld',
    '~/plugins/breakpoint.js',
    { src: '~plugins/ga.js', mode: 'client' }
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/sitemap',
    '@nuxtjs/robots',
    'cookie-universal-nuxt',
    '@nuxtjs/pwa'
  ],
  sitemap: {
    hostname: process.env.DOMINIO,
    gzip: true,
    exclude: [
      '/aviso-legal',
      '/politica-de-cookies',
      '/contacto'
    ],
    routes: [
      '/con-muebles/',
      '/con-sofa/',
      '/horizontal/',
      '/vertical/',
      '/de-matrimonio/',
      '/con-escritorio/',
      '/para-ninos/',
      '/barrera/',
      '/estilo-litera/',
      '/individual/'
    ]
  },
  robots: {
    UserAgent: '*',
    Disallow: [
      '/admin/',
      '/aviso-legal',
      '/politica-de-cookies',
      '/contacto'],
    Sitemap: process.env.DOMINIO + '/sitemap.xml'
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    proxy: true,
    credentials: true,
    debug: process.env.NODE_ENV !== 'production',
    https: process.env.NODE_ENV === 'production',
    retry: {
      retries: 3
    }
  },
  proxy: {
    '/api/': {
      target: process.env.NODE_ENV !== 'production' ? process.env.DOMINIO + ':8000/api' : process.env.DOMINIO + '/api',
      pathRewrite: { '^/api/': '' }
    }
  },
  pwa: {
    meta: {
      name: process.env.DEFAULT_TITLE,
      author: 'David Alarcón Segarra',
      description: process.env.DEFAULT_DESCRIPTION,
      lang: 'es'
    }
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    defaultAssets: false,
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  server: {
    port: '3000', // default: 3000
    host: process.env.NODE_ENV !== 'production' ? '127.0.0.1' : '0.0.0.0'
  },
  env: {
    image_path: process.env.NODE_ENV !== 'production' ? 'http://127.0.0.1:8000' : '',
    cookieAge: 60 * 60 * 24 * 30,
    dominio_json: process.env.DOMINIO,
    mail_json: process.env.MAIL,
    nicho_json: process.env.NICHO,
    ga_code_json: process.env.GA_CODE
  }
}
